using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMovement : MonoBehaviour
{
    public float movement_speed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        var horiz_movement = Input.GetAxis("Horizontal");
        var varvert_movement = Input.GetAxis("Vertical");
        transform.position += new Vector3(horiz_movement, varvert_movement, 0) * Time.deltaTime * movement_speed;
    }
}
